<?php

/**
 * @file
 * File to define theme functions.
 */

/**
 * Theme function for 'embedded' audio.
 */
function theme_link_field_formatter_audiofield($variables) {
  global $user;
  $file = $variables['file'];
  if (!$file) {
    return '';
  }

  if (!empty($file->uri)) {
    $audiofile = file_create_url($file->uri);
  }
  else {
    $audiofile = $file->url;
  }
  preg_match('/\.[^\.]+$/i', $audiofile, $ext);
  $op = $ext[0];
  $output = audiofield_get_player($audiofile, $op);

  return $output;
}

/**
 * Theme function for 'embedded' audio with embed tag.
 */
function theme_link_field_formatter_audiofield_embed($variables) {
  global $user;
  $file = $variables['file'];

  if (!$file) {
    return '';
  }

  if (!empty($file->uri)) {
    $audiofile = file_create_url($file->uri);
  }
  else {
    $audiofile = $file->url;
  }
  preg_match('/\.[^\.]+$/i', $audiofile, $ext);

  $output = '<audio controls>';
  $output .= '<source src="' . $audiofile . '" type="audio/mpeg">';
  $output .= '<embed height="50" width="100" src="' . $audiofile . '">';
  $output .= '</audio>';

  return $output;
}

/**
 * Returns HTML for an image field formatter.
 */
function theme_link_field_image_formatter($variables) {
  $item = $variables['item'];
  $output = '';

  $image = array(
    'path' => $item['url'],
  );

  if (array_key_exists('alt', $item)) {
    $image['alt'] = $item['alt'];
  }

  if (isset($item['attributes'])) {
    $image['attributes'] = $item['attributes'];
  }

  // Do not output an empty 'title' attribute.
  if (isset($item['title']) && drupal_strlen($item['title']) > 0) {
    $image['title'] = $item['title'];
  }
  if ($variables['image_style']) {
    $image_style = image_style_load($variables['image_style']);
    $image['width'] = $image_style['effects'][0]['data']['width'];
    $image['height'] = $image_style['effects'][0]['data']['height'];
  }
  $output = theme('image', $image);

  // The link path and link options are both optional, but for the options to be
  // processed, the link path must at least be an empty string.
  if (isset($variables['path']['path'])) {
    $path = $variables['path']['path'];
    $options = isset($variables['path']['options']) ? $variables['path']['options'] : array();
    // When displaying an image inside a link, the html option must be TRUE.
    $options['html'] = TRUE;
    $output = l($output, $path, $options);
  }

  return $output;
}

/**
 * Theme function for video.
 */
function theme_link_field_formatter_video($variables) {
  $file = $variables['video'];
  $attributes = array();
  if (!empty($variables['attributes'])) {
    $values = array_values($variables['attributes']);
    foreach ($values as $value) {
      if (!is_numeric($value)) {
        $attributes[] = $value;
      }
    }
  }

  $height = $variables['height'];
  $width = $variables['width'];
  $video_style = $variables['video_style'];

  if (!$file) {
    return '';
  }

  if (!empty($file->uri)) {
    $videofile = file_create_url($file->uri);
  }
  else {
    $videofile = $file->url;
  }
  preg_match('/\.[^\.]+$/i', $videofile, $ext);

  if ($video_style == 'vzaar') {
    $output = '<video width="' . $width . '" height="' . $height . '" ' . implode(' ', $attributes) . '>
      <source src="' . $videofile . '/video">' . t('Your browser does not support the video tag.') . '</video>';
  }
  else {
    $output = '<video width="' . $width . '" height="' . $height . '" ' . implode(' ', $attributes) . '>
      <source src="' . $videofile . '" type="video/' . $video_style . '">' . t('Your browser does not support the video tag.') . '</video>';
  }

  return $output;
}

/**
 * Theme function for pdf viewer.
 */
function theme_link_field_formatter_pdf($variables) {
  $file = $variables['file'];

  if (!$file) {
    return '';
  }

  if (!empty($file->uri)) {
    $file_url = file_create_url($file->uri);
  }
  else {
    $file_url = $file->url;
  }

  $library = libraries_load('pdf.js');
  if ($library['loaded'] == FALSE) {
    drupal_set_message($library['error message'], 'error');
    $download_str = t('Please download and install');
    return $download_str . ' ' . l($library['name'], $library['download url']) . '!';
  }

  $module_path = drupal_get_path('module', 'pdf');
  $library_path = libraries_get_path('pdf.js');

  $iframe_src = file_create_url($library_path . '/web/viewer.html') . '?href=' . $file_url;
  $force_pdfjs = 1;
  $content = array(
    '#type' => 'html_tag',
    '#tag' => 'iframe',
    '#value' => $file_url,
    '#attributes' => array(
      'class' => array('pdf'),
      'webkitallowfullscreen' => '',
      'mozallowfullscreen' => '',
      'allowfullscreen' => '',
      'frameborder' => 'no',
      'width' => '100%',
      'height' => '500px',
      'src' => $iframe_src,
      'data-src' => $file_url,
    ),
  );
  if ($force_pdfjs != TRUE) {
    drupal_add_js($module_path . '/js/acrobat_detection.js');
    drupal_add_js($module_path . '/js/default.js');
  }

  return render($content);
}
