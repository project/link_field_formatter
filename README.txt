README.txt
==========

INTRODUCTION
------------
Link as Document with Embedded Google Doc Viewer:
This module provides a new format for the Link field type. This format presents
the file as a fully rendered object within a web page - i.e. it displays the 
contents of the link as appropriate to its filetype (Adobe Acrobat .pdf, 
Microsoft Word .doc/.docx, Micrososft Excel .xls/.xlsx, Microsoft Powerpoint 
.ppt/.pptx), using the Google Docs embedded rendering engine.
N.B.: Only link having .pdf,.doc/.docx, .xls/.xlsx. and .ppt/.pptx may use this 
formatter - Google Docs must be able to access the link url in order to render 
and display it. In other words, it won't work on a typical development laptop, 
or if your server is behind a firewall where Google is unable to access it.

Link as Image:
Also this module helps play audio files and render images hosted on another
server.

Link as Document with Embedded Doc Viewer(IFRAME):
Added new formatter to view uploaded pdf file and pdf url along with slideshare
url to view ppt.

Link as Audio:
Added new formatter to play audio with default configured player or with audio
tag.

Link as Video:
Added new formatter to play video with video tag. Added support to vzaar url's.

REQUIREMENTS
------------
This module requires the following modules:
 * Link (https://drupal.org/project/link)
 * Embedded Google Docs Viewer (https://drupal.org/project/gdoc_field)
 * AudioField (https://drupal.org/project/audiofield)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
1. Embedded Google Doc Viewer: To use this module for Document URL, add a
Link field to a new or existing content type (such as Basic Page) on the
content type's Manage Fields form. The Link field type provides only one
widget type - Link - so select that. On the content type's "Manage Display"
form, there will be a drop-down select list of available display formats for
the Link field. To display the link url within the embedded Google Docs viewer,
choose the 'Link - Google Embedded Document Viewer' format. The document viewer
may be styled using the CSS selector '.gdoc-field'. By default, the viewer's
width is 100% and its height is 400px, with a 1px black border.

2. Audio: To use this field as an audio formatter, add a Link field to a new or
existing content type (such as Basic Page) on the content type's Manage Fields
form. The Link field type provides only one widget type - Link - so select that.
On the content type's "Manage Display" form, there will be a drop-down select
list of available display formats for the Link field. To display the link url
within the default audio player configured by audiofield module. Choose the
'Link - Audio player' format.

3. Image: To use this field as an image formatter, add a Link field to a new
or existing content type (such as Basic Page) on the content type's
Manage Fields form. The Link field type provides only one widget type - Link
- so select that. On the content type's "Manage Display" form, there will
be a drop-down select list of available display formats for the Link field.
To display the image, choose the 'Link - Image' format.

4. Embedded Doc Viewer: To use this field add a Link field to a new or existing
content type (such as Basic Page) on the content type's Manage Fields form.
The Link field type provides only one widget type - Link - so select that.
On the content type's "Manage Display" form, where there will be a drop-down
select list of available display formats for the Link field, select
"Link - Embedded Docs Viewer".

5. Video: To use this field add a Link field to a new or existing
content type (such as Basic Page) on the content type's Manage Fields form.
The Link field type provides only one widget type - Link - so select that.
On the content type's "Manage Display" form, where there will be a drop-down
select list of available display formats for the Link field, select "Link - Video".
Click on settings icon next to the drop down. It will allow you to select
"Video style", "Attributes", "Height" and "width" of the video player.
Select appropriate values and click "Update".

MAINTAINERS
-----------
Current maintainers:
 * Mandar Bhagwat (mandarmbhagwat78)-https://www.drupal.org/u/mandarmbhagwat78
